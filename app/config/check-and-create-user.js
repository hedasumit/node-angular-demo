//User Service
var User = require('../modules/models/user.model.js').User;
//create user Object
module.exports = {
    /**
     * Method that check if there is no users then create a super admin
     */
    createAndCheck: function () {
        User.count().then(function (count) {
            if (count < 1) {
                var userObj = new User();
                userObj.name = "Test Heda";
                userObj.email = "test@demo.com";
                userObj.password = userObj.generateHash('12345678');
                userObj.save(userObj).then(function (user) {
                    console.log('Default User created');
                })
                    .catch(function (err) {
                        console.log('err', err);
                    });
            }
        })
            .catch(function (err) {
                console.log('err', err);
            });

    }

};
