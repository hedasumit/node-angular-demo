var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var productSchema = Schema({
    name: {type: String},
    category: {
        type: Schema.ObjectId,
        ref: 'Category',
        required: true
    },
    image: {type: String},
    price: {type: Number},
    discount: {type: Number},
    netPrice: {type: Number},
    description: {type: String}
});


var Product = mongoose.model('Product', productSchema);
module.exports.Product = Product;
