var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var categorySchema = Schema({
    name: {type: String}
});


var Category = mongoose.model('Category', categorySchema);
module.exports.Category = Category;
