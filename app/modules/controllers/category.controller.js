"use strict";
var ObjectId = require('mongoose').Types.ObjectId,
    Category = require('../models/category.model.js').Category,
    apiCodes = require('../../config/api-codes.js');

module.exports = function (app) {


    app.getCategories = function (req, res) {
        Category.find().exec(function (err, categories) {
            if (err) {
                return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Categories not Found'});
            }
            if (!categories) {
                return res.status(apiCodes.BAD_REQUEST).send({message: 'Categories not Available'});

            }
            else {
                return res.status(apiCodes.SUCCESS).send({data: categories});
            }
        });
    };

    app.createCategory = function (req, res) {
        if (req.body.name) {
            Category.findOne({name: req.body.name}).exec(function (err, category) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Categories not Found'});
                }
                else if (category) {
                    return res.status(apiCodes.DUPLICATE).send({message: 'Category Already Exit'});

                }
                else {
                    var newCategory = new Category();
                    newCategory.name = req.body.name;
                    newCategory.save(function (err, data) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in creating category'});
                        }
                        if (data) {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Category Created'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({message: 'Name Required'});
        }
    };


    app.getCategoryById = function (req, res) {
        if (req.params.categoryId) {
            Category.findById(req.params.categoryId).exec(function (err, categoryObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting category'});
                }
                else if (!categoryObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Categories not Available'});
                } else {
                    return res.status(apiCodes.SUCCESS).send({
                        data: categoryObject
                    });
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Category id Not provided'
            });
        }

    };


    app.updateCategoryById = function (req, res) {
        if (req.params.categoryId) {
            Category.findById(req.params.categoryId).exec(function (err, categoryObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting category'});
                }
                else if (!categoryObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Categories not Available'});
                } else {
                    categoryObject.name = req.body.name;
                    categoryObject.save(function (err, response) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({
                                message: 'Error in updating category'
                            });
                        } else {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Category updated'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Category id Not provided'
            });
        }

    };

    app.deleteCategoryById = function (req, res) {
        if (req.params.categoryId) {
            Category.findById(req.params.categoryId).exec(function (err, categoryObject) {
                if (err) {
                    return res.status(apiCodes.INTERNAL_ERROR).send({message: 'Error in getting category'});
                }
                else if (!categoryObject) {

                    return res.status(apiCodes.BAD_REQUEST).send({message: 'Categories not Available'});
                } else {
                    categoryObject.remove(function (err, response) {
                        if (err) {
                            return res.status(apiCodes.INTERNAL_ERROR).send({
                                message: 'Error in deleting category'
                            });
                        } else {
                            return res.status(apiCodes.SUCCESS).send({
                                message: 'Category Deleted'
                            });
                        }
                    })
                }
            });
        } else {
            return res.status(apiCodes.BAD_REQUEST).send({  // Email is not provided in body
                message: 'Category id Not provided'
            });
        }
    };




};