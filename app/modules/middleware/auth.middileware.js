/**
 * This file provides middleware for authorization
 * @type {exports|module.exports}
 */
var tokenService = require('../utilities/jwt.token.service.js'),
    User = require('../models/user.model.js').User,
    apiCodes = require('../../config/api-codes.js');

module.exports = function (app) {
    /**
     * This function checks if user is authenticated or not,
     */
    app.ensureAuthenticated = function(req, res, next) {
        var token;
        if (req.headers && req.headers.authorization) {
            var parts = req.headers.authorization.split(' ');
            if (parts.length == 2) {
                var scheme = parts[0],
                    credentials = parts[1];
                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
            } else {
                return res.json(apiCodes.UNAUTHORIZED, {message: 'Invalid Format'});
            }
        } else if (req.param('token')) {
            token = req.param('token');
            // We delete the token from query and body to not mess with blueprints
            delete req.query.token;
            delete req.body.token;
        } else {
            return res.status(apiCodes.UNAUTHORIZED).send({
                message: 'No Authorization header present'
            });
        }

        tokenService.verifyToken(token, function(error, token) {
            if (error) {
                return res.json(apiCodes.UNAUTHORIZED, {message: 'User not Found'});
            } else {
                req.token = token;
                User.findById(token.id).exec(function (err, user) {
                    if (err) return next(err);
                    if (!user) return next(new Error('User not Found' + id));
                    req.user = user;
                    return next();
                });
            }
        });
    };


};
