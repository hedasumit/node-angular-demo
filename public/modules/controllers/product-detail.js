


'use strict';
angular.module('app')
    .controller('ProductDetailCtrl', function ($scope, $http, $state, toaster) {


        $scope.product = {};

        $scope.getProduct = function () {
            $http.get('/api/v1/product/'+$state.params.productId).then(function (result) {
                $scope.product = result.data.data;
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.init = function () {
            $scope.getProduct();
        };

        $scope.init();

    });
