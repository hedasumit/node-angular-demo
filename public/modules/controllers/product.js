'use strict';
angular.module('app')
    .controller('ProductCtrl', function ($scope, $http, $state, toaster) {


        $scope.createProduct = true;
        $scope.products = [];
        $scope.product = {};

        $scope.saveProduct = function () {
            $scope.product.category = $state.params.categoryId;
            var file = $scope.myFile;
            var payload = new FormData();
            payload.append("name", $scope.product.name);
            payload.append("category", $scope.product.category);
            payload.append('netPrice', $scope.product.netPrice || 0);
            payload.append('price', $scope.product.price || 0);
            payload.append('discount', $scope.product.discount || 0);
            payload.append('description', $scope.product.description || '');
            payload.append('image', file);
            $http({
                url: '/api/v1/products',
                method: 'POST',
                data: payload,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).then(function (result) {
                $("#createProduct").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };

        $scope.setProduct = function (product, update) {
            $scope.createProduct = false;
            $scope.product = product;
            $scope.product.category = $state.params.categoryId;
            if (update) {
                $("#createProduct").modal('show');
            } else {
                $("#deleteProductModal").modal('show');
            }

        };

        $scope.updateProduct = function () {


            $scope.product.category = $state.params.categoryId;
            var file = $scope.myFile;
            var payload = new FormData();
            payload.append("_id", $scope.product._id);
            payload.append("name", $scope.product.name);
            payload.append("category", $scope.product.category);
            payload.append('netPrice', $scope.product.netPrice || 0);
            payload.append('price', $scope.product.price || 0);
            payload.append('discount', $scope.product.discount || 0);
            payload.append('description', $scope.product.description || '');
            payload.append('image', file);
            $http({
                url: '/api/v1/product/'+ $scope.product._id,
                method: 'PUT',
                data: payload,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).then(function (result) {
                $("#createProduct").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.deleteProduct = function () {
            $http.delete('/api/v1/product/' + $scope.product._id).then(function (result) {
                $("#deleteProductModal").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };

        $scope.getProducts = function () {
            $http.get('/api/v1/categories/product/'+$state.params.categoryId).then(function (result) {
                $scope.products = result.data.data;
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.init = function () {
            $scope.createProduct = true;
            $scope.product = {};
            $scope.getProducts();
        };

        $scope.init();

    });
