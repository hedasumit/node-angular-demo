'use strict';
angular.module('app')
    .controller('ProductListCtrl', function ($scope, $http, $state, toaster) {


        $scope.products = [];
        $scope.pagination = {
            currentPage: 1
        };
        $scope.itemPerPages = [10, 25, 50, 100];
        $scope.itemPerPage = 10;
        $scope.selectedItems = 0;

        $scope.getItemPerPage = function (items, index) {
            $scope.selectedItems = index;
            $scope.itemPerPage = items;
            $scope.getProducts();
        };

        $scope.getProducts = function () {
            $http.get('/api/v1/products?page=' + $scope.pagination.currentPage + '&limit=' + $scope.itemPerPage).then(function (result) {
                $scope.products = result.data.data;
                $scope.pagination.totalItems = result.data.pagination.count;
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.goToProductDetail = function (productId) {
            $state.go('dashboard.product-detail', {productId: productId});
        };


        $scope.init = function () {
            $scope.getProducts();
        };

        $scope.init();

    });
