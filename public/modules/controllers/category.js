'use strict';
angular.module('app')
    .controller('CategoryCtrl', function ($scope, $http, $state,toaster) {


        $scope.createCategory = true;
        $scope.categories = [];
        $scope.category = {};
        $scope.saveCategory = function () {
            $http.post('/api/v1/categories', $scope.category).then(function (result) {
                $("#createCategory").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };

        $scope.setCategory = function (category, update) {
            $scope.createCategory = false;
            $scope.category = category;
            if (update) {
                $("#createCategory").modal('show');
            } else {
                $("#deleteCategoryModal").modal('show');
            }

        };

        $scope.updateCategory = function () {
            $http.put('/api/v1/category/' + $scope.category._id, $scope.category).then(function (result) {
                $("#createCategory").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.deleteCategory = function () {
            $http.delete('/api/v1/category/' + $scope.category._id).then(function (result) {
                $("#deleteCategoryModal").modal('hide');
                $scope.init();
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };

        $scope.getCategories = function () {
            $http.get('/api/v1/categories').then(function (result) {
                $scope.categories = result.data.data;
            }).catch(function (err) {
                toaster.error(err.data.message);
            });
        };


        $scope.goToProducts = function(categoryId){
                $state.go('dashboard.product',{categoryId: categoryId});
        };

        $scope.init = function () {
            $scope.createCategory = true;
            $scope.getCategories();
        };

        $scope.init();

    });
