'use strict';

angular.module('app').run(['$rootScope', '$state', '$auth',
    function ($rootScope, $state, $auth) {

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams, options) {
                if (toState.data) {
                    var isAuthenticate = toState.data.authenticate;
                    if (isAuthenticate) {
                        if (!$auth.isAuthenticated()) {
                            event.preventDefault();
                            $state.transitionTo('login');
                        }
                    }
                }
            });
    }]);

    
