'use strict';
/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
    .module('app', [
        'ui.router',
        'ui.bootstrap',
        'angular-loading-bar',
        'satellizer',
        'toaster',
        'ngAnimate'
    ])
    .config(['$stateProvider', '$urlRouterProvider', '$authProvider','$compileProvider',
        function ($stateProvider, $urlRouterProvider, $authProvider, $compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|ftp|blob):|data:image\//);
            $authProvider.loginUrl = '/api/v1/auth/login';
            $authProvider.tokenType = 'Bearer';


            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('dashboard', {
                    url: '/app',
                    templateUrl: 'views/dashboard/main.html',
                    data: {
                        authenticate: true
                    }
                })
                .state('dashboard.home', {
                    url: '/home',
                    controller: 'MainCtrl',
                    templateUrl: 'views/dashboard/home.html',
                    data: {
                        authenticate: true
                    }
                })
                .state('login', {
                    url: '/login',
                    controller: 'LoginCtrl',
                    templateUrl: 'views/pages/login.html'

                })
                .state('dashboard.category', {
                    templateUrl: 'views/pages/category.html',
                    controller: 'CategoryCtrl',
                    url: '/category',
                    data: {
                        authenticate: true
                    }
                })
                .state('dashboard.product', {
                    templateUrl: 'views/pages/product.html',
                    controller: 'ProductCtrl',
                    url: '/product/:categoryId',
                    data: {
                        authenticate: true
                    }
                })
                .state('dashboard.product-list', {
                    templateUrl: 'views/pages/product-list.html',
                    controller: 'ProductListCtrl',
                    url: '/products',
                    data: {
                        authenticate: true
                    }
                })
                .state('dashboard.product-detail', {
                    templateUrl: 'views/pages/product-detail.html',
                    controller: 'ProductDetailCtrl',
                    url: '/products/:productId',
                    data: {
                        authenticate: true
                    }
                })
        }]);

    
